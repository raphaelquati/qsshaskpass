#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <windows.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setArgs(int argc, char *argv[])
{
    if (argc > 1) {
        ui->label->setText(QString("%1").arg(argv[1]));
    }
}

void MainWindow::on_pb_ok_clicked()
{
    DWORD count;

    QString pass = ui->lineEdit->text() + "\n";

    if ( !ui->lineEdit->text().isEmpty() ) {
        HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
        WriteFile(hStdout, pass.toLatin1().constData(), pass.length(), &count, NULL);
        exit(0);
    }
}
